#include "main.h"

#define BUF_LEN 50

void push(uint8_t c);
uint8_t pop(void);
uint8_t isNumber(uint8_t *c);
uint8_t isSpace(uint8_t *p);
int16_t getNum(uint8_t *p);
int8_t* checkCommand(uint8_t *str, uint8_t *etalon);
void commandParam(uint8_t *commandNames[], uint8_t commandNumber, int16_t returnArr[], uint8_t arrLen);
void clearBuf(void);
