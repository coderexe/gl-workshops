#include "stm32f4xx_hal.h"



#define PRE_SCALE_MIN 		0x03
#define PRE_SCALE_MAX 		0xFF
#define PWM_FREQ_MIN 		24
#define PWM_FREQ_MAX 		1526
#define LED_NUM				16
#define BYTES_PER_LED		4
#define INT_CLC_FREQ		25000000
#define TRANSMIT_TIMEOUT	50

void PCA_Sleep(I2C_HandleTypeDef *hi2c, uint8_t devID, uint8_t sleepmode);
uint8_t PCA_getCtrReg(I2C_HandleTypeDef *hi2c, uint8_t devID, uint8_t regAddr);
void PCA_OperationEnable(I2C_HandleTypeDef *hi2c, uint8_t devID, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, uint8_t yes);
uint8_t PCA_setFreq(I2C_HandleTypeDef *hi2c, uint8_t devID, uint16_t frequency);
void PCA_Init(I2C_HandleTypeDef *hi2c, uint8_t devID, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
uint8_t PCA_PWM_Set(I2C_HandleTypeDef *hi2c, uint8_t devID, uint16_t duty_cycle, uint8_t led);
void PCA_PWM_SetAll(I2C_HandleTypeDef *hi2c, uint8_t devID, uint16_t duty_cycle);
