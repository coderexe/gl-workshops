#include "PCA9685.h"

#define	MODE1				0x00	//Mode register 1
#define MODE2				0X01	//Mode register 2
#define SUBART1				0x02	//I2C - bus subaddress 1
#define SUBART2				0x03	//I2C - bus subaddress 2
#define SUBART3				0x04	//I2C - bus subaddress 3
#define ALLCALLADR			0x05	//LED All Call I2C-bus address
#define LED0_ON_L			0x06	//LED0 output and brightness control byte 0
#define LED0_ON_H			0x07	//LED0 output and brightness control byte 1
#define LED0_OFF_L			0x08	//LED0 output and brightness control byte 2
#define LED0_OFF_H			0x09	//LED0 output and brightnes controll byte 3
#define ALL_LED_ON_L		0xFA	//load all the LEDn_ON registers, byte 0
#define ALL_LED_ON_H		0xFB	//load all the LEDn_ON registers, byte 1
#define ALL_LED_OFF_L		0xFC	//load all the LEDn_OFF registers, byte 0
#define ALL_LED_OFF_H		0xFD	//load all the LEDn_OFF registers, byte 1
#define PRE_SCALE			0xFE	//prescaler for PWM output frequency
#define TestMode			0xFF	//defines teh test mode to be entered


#define RESET			0x00
#define getSlaveReadId(ID)	(ID |= 0x01) 
#define lower_byte(a) (a & 0xFF)
#define upper_byte(a) (a >> 8)

enum
{
	false = 0,
	true = 1
};

enum
{
	ALLCALL		= 0x01,
	SUB3 		= 0x02,
	SUB2 		= 0x04,
	SUB1		= 0x08,
	SLEEP		= 0x10,
	AI			= 0x20,
	EXTCLC		= 0x40,
	RESTART		= 0x80

}typedef MODE1_Bits;




uint8_t PCA_getCtrReg(I2C_HandleTypeDef *hi2c, uint8_t devID, uint8_t regAddr)
{
	uint8_t ctrlReg;
	getSlaveReadId(devID);
	HAL_I2C_Mem_Read(hi2c, devID, regAddr, 1, &ctrlReg, 1, TRANSMIT_TIMEOUT);
	return ctrlReg;
}

void PCA_OperationEnable(I2C_HandleTypeDef *hi2c, uint8_t devID, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, uint8_t yes)
{
	if(yes)
		HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_SET);
}


void PCA_Init(I2C_HandleTypeDef *hi2c, uint8_t devID, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);
	HAL_Delay(10);

	uint8_t buf[]={MODE1, ALLCALL};

	HAL_I2C_Master_Transmit(hi2c, devID, buf, sizeof(buf), TRANSMIT_TIMEOUT); //not needed

}


uint8_t PCA_PWM_Set(I2C_HandleTypeDef *hi2c, uint8_t devID, uint16_t duty_cycle, uint8_t led)
{
	
	if (led < LED_NUM && duty_cycle <= 100)
	{		
		uint16_t ledOnTime;
		if(duty_cycle == 0)
			ledOnTime = 0;
		else
			ledOnTime = (4096 * duty_cycle) / 100 - 1;
		uint8_t buf[2];
		buf[0] = LED0_ON_L + led * BYTES_PER_LED; //get first required register
		uint8_t regBuf[BYTES_PER_LED] = {0, 0, (uint8_t)lower_byte(ledOnTime), (uint8_t)upper_byte(ledOnTime)};

		for(uint8_t i = 0; i < BYTES_PER_LED; i++)
		{
			buf[1] = regBuf[i]; // set value for register

			HAL_I2C_Master_Transmit(hi2c, devID, buf, sizeof(buf), TRANSMIT_TIMEOUT);

			buf[0]++; // next register
		}

		return true;
	}
	else
		return false;
}


void PCA_PWM_SetAll(I2C_HandleTypeDef *hi2c, uint8_t devID, uint16_t duty_cycle)
{
	uint16_t ledOnTime = (4096 * duty_cycle) / 100 - 1;
	uint8_t regBuf[BYTES_PER_LED] = {0, 0, (uint8_t)lower_byte(ledOnTime), (uint8_t)upper_byte(ledOnTime)};
	uint8_t buf[2];
	buf[0] = ALL_LED_ON_L;
	for(uint8_t i = 0; i < BYTES_PER_LED; i++)
	{
		buf[1] = regBuf[i];
		HAL_I2C_Master_Transmit(hi2c, devID, buf, sizeof(buf), TRANSMIT_TIMEOUT);
		buf[0]++; // next register
	}
}


void PCA_Sleep(I2C_HandleTypeDef *hi2c, uint8_t devID, uint8_t sleepmode)
{
	uint8_t ctrReg, buf[2];
	buf[0] = MODE1;
	getSlaveReadId(devID);
	ctrReg = PCA_getCtrReg(hi2c, devID, MODE1);
	if(sleepmode)
	{
		buf[1] = ctrReg | SLEEP;
		HAL_I2C_Master_Transmit(hi2c, devID, buf, sizeof(buf), TRANSMIT_TIMEOUT);
	}
	else if(ctrReg & SLEEP)
	{

		ctrReg &= (~SLEEP);
		buf[1] = ctrReg;
		HAL_I2C_Master_Transmit(hi2c, devID, buf, sizeof(buf), TRANSMIT_TIMEOUT);

		HAL_Delay(1);	// 500 mcs for oscillator to stabilize

		ctrReg &= (~RESTART);
		buf[1] = ctrReg;
		HAL_I2C_Master_Transmit(hi2c, devID, buf, sizeof(buf), TRANSMIT_TIMEOUT);
	}
}



uint8_t PCA_setFreq(I2C_HandleTypeDef *hi2c, uint8_t devID, uint16_t frequency)
{
	uint8_t prescaler;
	if(frequency > PWM_FREQ_MIN && frequency <= PWM_FREQ_MAX)
		prescaler = INT_CLC_FREQ / (4096 * frequency) - 1;
	else if(frequency == PWM_FREQ_MIN)
		prescaler = PRE_SCALE_MAX;
	else
		return false;

	PCA_Sleep(hi2c, devID, true);

	uint8_t buf[2];
	buf[0] = PRE_SCALE;
	buf[1] = prescaler;
	HAL_I2C_Master_Transmit(hi2c, devID, buf, 2, TRANSMIT_TIMEOUT);

	PCA_Sleep(hi2c, devID, false);

	return true;
}


/*
void PCA_Sleep(I2C_HandleTypeDef *hi2c, uint8_t devID, uint8_t sleep)
{
	uint8_t TxBuffer[2];
	TxBuffer[0] = MODE1;
	if (sleep)
	{
		TxBuffer[1] =  SLEEP;
		HAL_I2C_Master_Transmit(hi2c, devID, (uint8_t*) &TxBuffer, 2, TRANSMIT_TIMEOUT);
	}
	else
	{
		TxBuffer[1] =  RESET;
		HAL_I2C_Master_Transmit(hi2c, devID, (uint8_t*) &TxBuffer, 2, TRANSMIT_TIMEOUT);
		TxBuffer[1] = RESTART | ALLCALL;
		HAL_I2C_Master_Transmit(hi2c, devID, (uint8_t*) &TxBuffer, 2, TRANSMIT_TIMEOUT);
	}
}


uint8_t PCA_setFreq(I2C_HandleTypeDef *hi2c, uint8_t devID, uint16_t frequency)
{
	uint8_t prescaler;
	if(frequency > PWM_FREQ_MIN && frequency <= PWM_FREQ_MAX)
		prescaler = INT_CLC_FREQ / (4096 * frequency) - 1;
	else if(frequency == PWM_FREQ_MIN)
		prescaler = PRE_SCALE_MAX;
	else
		return false;
	uint8_t buf[] = {MODE1, SLEEP};
	HAL_I2C_Master_Transmit(hi2c, devID, (uint8_t*) &buf, 2, TRANSMIT_TIMEOUT);

	buf[0] = PRE_SCALE, buf[1] = prescaler;
	HAL_I2C_Master_Transmit(hi2c, devID, (uint8_t*) &buf, 2, TRANSMIT_TIMEOUT);

	buf[0] = MODE1, buf[1] = RESET;
	HAL_I2C_Master_Transmit(hi2c, devID, (uint8_t*) &buf, 2, TRANSMIT_TIMEOUT);

	buf[1] = RESTART | ALLCALL;
	HAL_I2C_Master_Transmit(hi2c, devID, (uint8_t*) &buf, 2, TRANSMIT_TIMEOUT);

	return prescaler;
}*/
