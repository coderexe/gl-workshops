#include "input.h"

uint8_t commandBuf[BUF_LEN], bufend = 0;


void push(uint8_t c)
{
	if(bufend < BUF_LEN)
		commandBuf[bufend++] = c;
}

uint8_t pop(void)
{
	if(bufend > 0)
	{
		return commandBuf[--bufend];;
	}
	else
		return false;
}

uint8_t isNumber(uint8_t *c)
{
	if( (*c) >= '0' && (*c) <= '9')
		return true;
	else
		return false;
}

uint8_t isSpace(uint8_t *p)
{
	if((*p == ' ') || (*p == '\t') || (*p == '\n'))
		return true;
	else
		return false;
}

int16_t getNum(uint8_t *p)
{

	if(!isNumber(p))
	{
		clearBuf();
		return error;
	}
	uint16_t num = 0;

	while((*p) == '0')
		p++;

	while(isNumber(p))
	{
		num = num * 10 + (*p - '0');
		p++;
	}

	if((*p) == '\0' || isSpace(p))
		return num;
	else
		return error;

}

int8_t* checkCommand(uint8_t *str, uint8_t *etalon)
{
	while(isSpace(str))
		str++;

	while((*etalon) != '\0')
	{
		if((*etalon) != (*str))
			return NULL;
		etalon++;
		str++;
	}
	
	if((*str) == '\0' || isSpace(str))
		return str;
	else
		return NULL;
}

void commandParam(uint8_t *commandNames[], uint8_t commandNumber, int16_t returnArr[], uint8_t arrLen)
{
	push('\0');
	uint8_t *p, command;
	for(command = 0; command < commandNumber; command++)
	{
		p = checkCommand(commandBuf, commandNames[command]);
		if(p != NULL)
			break;
	}

	if(command >= commandNumber)
	{
		clearBuf();
		returnArr[0] = error;
		return;
	}
	else
	{
		returnArr[0] = command;
	}
	for(uint8_t i = 1; i < arrLen; i++)
	{
		while(isSpace(p))
			p++;
		returnArr[i] = getNum(p);
		if(returnArr[i] == error)
			return;

		while(isNumber(p))
			p++;
	}
}

void clearBuf(void)
{
	bufend = 0;
	for(uint8_t i = 0; i < BUF_LEN; i++)
		commandBuf[i] = '\0';
}
