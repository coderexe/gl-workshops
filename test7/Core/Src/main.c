/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
				/* Commands */
#define READ25			0x03 //Read at 25MHz
#define READ50			0x0B //Read at 50MHz
#define ERASE4kB		0x52 //erase 4kB block
#define ERASE32kB		0xD8
#define ERASE_ALL		0x60 //or 0xC7
#define BYTE_PROG		0x02 //program 1 data byte
#define AAI				0xAD //Auto Address Increment Programming
#define RDSR			0x05 //Read-Status-Register
#define EWSR			0x50 //Enable-Write-Status-Register
#define WRSR			0x01 //Write-Status-Register
#define WREN			0x06 //Write-Enable
#define WRDI			0x04 //Write-Disable
#define RDID			0xAB //Read ID
#define EBSY			0x70
#define DBSY			0x80
				/*************/
#define LAST_ADDR		0x1FFFFF
#define FIRST_ADDR		0

#define SPI_TIMEOUT		100
#define UART_TIMEOUT	10
#define STR_NUM			20
#define STR_LEN			150
#define SECTION			0x1000
#define NO_VALUE		0xFF

#define INFO			"READ: 0, CLEAR: 1, WRITE: 2\r\n"

enum
{
	false,
	true
};
enum
{
	READ = '0',
	ERASE = '1',
	WRITE = '2'
};

uint8_t  readStReg(void);
uint32_t strlen(uint8_t *p);
uint8_t ReadID(void);
uint8_t readByte50(uint32_t addr);
void showData(void);
void disableProtection(void);
void eraseAllData(void);
uint8_t  readStReg(void);
void writeByte(uint32_t addr, uint8_t c);
void writeData(void);

const uint8_t memoryCapsule[STR_NUM][STR_LEN] = {
		"From: Dmytro Tsuman, dimatsuman@gmail.com\r\n",
		"Mentor: Denys Kondratenko, denys.kondratenko@globallogic.com\r\n",
		"Date: 06.12.2021\r\n",
		"TIME CAPSULE\r\n",
		"Times change, people change more",
		"Know yourself and we will win this war",
		"Too many problems, not enough solutions",
		"You're in the equation but your nothing but a nuisance",
		"A nuisance",
		"We're gonna change your mind",
		"Look inside yourself and find hate, find fear",
		"Your life is over here",
		"You want to fade from time",
		"Reaching out, from the inside",
		"Your face will disappear with years",
		"In a dying world",
		"We are so much more",
		"Than money, and clothes, and things we can't afford",
		"You must find a way",
		"To break down this door"
	};
const uint8_t str[] = "Hello, I am Dima";
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#define startSPI()		HAL_GPIO_WritePin(CE_GPIO_Port, CE_Pin, GPIO_PIN_RESET)
#define stopSPI()		HAL_GPIO_WritePin(CE_GPIO_Port, CE_Pin, GPIO_PIN_SET)

//checks the state of the device
#define isBusy()		(readStReg() & 0x01)

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
static void MX_USART3_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint32_t strlen(uint8_t *p)
{
	uint32_t len = 0;
	for(; (*p) != '\0'; p++)
		len++;
	return len;
}

uint8_t ReadID(void)
{
	uint8_t rcv[8] = {0x0}, trx[8] = { 0x0 };
	trx[0] = RDID;
	startSPI();
	HAL_SPI_TransmitReceive(&hspi1, trx, rcv, 8, SPI_TIMEOUT);
	stopSPI();
	return rcv;
}

uint8_t readByte50(uint32_t addr)
{
	uint8_t trx[5], rcv;
	trx[0] = READ50;
	trx[4] = 0; //dummy byte
	trx[1] = (addr & 0xFF0000) >> 16;
	trx[2] = (addr & 0xFF00) >> 8;
	trx[3] = (addr & 0xFF);
	startSPI();
	HAL_SPI_Transmit(&hspi1, trx, sizeof(trx)/sizeof(trx[0]), SPI_TIMEOUT);
	HAL_SPI_Receive(&hspi1, &rcv, sizeof(rcv), SPI_TIMEOUT);
	stopSPI();
	return rcv;
}

void showData(void)
{
	uint8_t buf[STR_LEN], bufend = 0, c = 0, isEmpty = true;
	uint32_t addr;
	for(uint32_t j = 0; j < STR_NUM; j++)
	{
		addr = FIRST_ADDR + SECTION * j;

		for(uint32_t i = 0; i < STR_LEN; i++)
		{
			while(isBusy())
				;
			c = readByte50(addr + i);

			if(c == '\0' || c == NO_VALUE)
				break;
			else
				buf[bufend++] = c;
		}

		if(bufend != 0)
		{
			buf[bufend++] = '\r';
			buf[bufend] = '\n';
			HAL_UART_Transmit(&huart3, buf, bufend + 1, UART_TIMEOUT);
			bufend = 0;
			isEmpty = false;
		}
	}
	if(isEmpty)
		HAL_UART_Transmit(&huart3, (uint8_t*) "EMPTY\r\n", 5+2, UART_TIMEOUT);
}
void disableProtection(void)
{
	uint8_t buf[] = {EWSR, 0};
	//enable changes to status register
	startSPI();
	HAL_SPI_Transmit(&hspi1, buf, sizeof(buf[0]), SPI_TIMEOUT);
	stopSPI();

	//disable protection
	buf[0] = WRSR;
	startSPI();
	HAL_SPI_Transmit(&hspi1, buf, sizeof(buf)/sizeof(buf[0]), SPI_TIMEOUT);
	stopSPI();
}
void eraseAllData(void)
{
	disableProtection();

	uint8_t command = WREN;

	while(isBusy())
		;

	startSPI();
	HAL_SPI_Transmit(&hspi1, &command, sizeof(command), SPI_TIMEOUT);
	stopSPI();

	command = ERASE_ALL;
	while(isBusy())
		;

	startSPI();
	HAL_SPI_Transmit(&hspi1, &command, sizeof(command), SPI_TIMEOUT);
	stopSPI();

	while(isBusy())
		;
}

void writeByte(uint32_t addr, uint8_t c)
{
	if(addr > LAST_ADDR)
		return;
	uint8_t trx[5];

	trx[0] = WREN;
	startSPI();
	HAL_SPI_Transmit(&hspi1, trx, sizeof(trx[0]), SPI_TIMEOUT);
	stopSPI();

	trx[0] = BYTE_PROG;
	trx[1] = (addr & 0xFF0000) >> 16;
	trx[2] = (addr & 0xFF00) >> 8;
	trx[3] = (addr & 0xFF);
	trx[4] = c;


	startSPI();
	HAL_SPI_Transmit(&hspi1, trx,  5, SPI_TIMEOUT);
	stopSPI();
}

void writeData(void)
{
	uint32_t addr;
	for(uint16_t j = 0; j < STR_NUM; j++)
	{
		addr = FIRST_ADDR + SECTION * j;
		for(uint16_t i = 0; i < strlen(&memoryCapsule[j][0]) + 1; i++)
		{
			while(isBusy())
					;
			writeByte(addr, memoryCapsule[j][i]);
			addr++;
		}
	}
		while(isBusy())
				;
}

uint8_t  readStReg(void)
{
	uint8_t trx = RDSR, rcv = 0;

	startSPI();
	HAL_SPI_Transmit(&hspi1, &trx, sizeof(rcv), SPI_TIMEOUT);
	HAL_SPI_Receive(&hspi1, &rcv, sizeof(rcv), SPI_TIMEOUT);
	stopSPI();

	return rcv;
}

void getINFO(void)
{
	uint8_t *p = INFO;
	HAL_UART_Transmit(&huart3, p, strlen(p), UART_TIMEOUT);
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  // starting up the device
  stopSPI();
  HAL_Delay(100);
  ReadID();
  HAL_Delay(100);

  getINFO();

  uint8_t UART_Rcv;
  while (1)
  {

	  if(HAL_UART_Receive(&huart3, &UART_Rcv, 1, UART_TIMEOUT) == HAL_OK)
	  {

		  switch(UART_Rcv)
		  {

		  case READ:
			  showData();
			  getINFO();
			  break;

		  case ERASE:
			  eraseAllData();
			  break;

		  case WRITE:
			  eraseAllData();
			  writeData();
			  break;

		  default:
			  HAL_UART_Transmit(&huart3, (uint8_t*)"Wrong input\r\n", 11+2, UART_TIMEOUT);
			  getINFO();
			  break;
		  }
	  }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(CE_GPIO_Port, CE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : CE_Pin */
  GPIO_InitStruct.Pin = CE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(CE_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

